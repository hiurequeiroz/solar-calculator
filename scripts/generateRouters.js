const fs = require('fs')
const path = require('path')
const scrapeIt = require('scrape-it')
const throttle = require('lodash.throttle')

const basePage = 'https://openwrt.org/toh'

async function scrapeRouterList () {
  return scrapeIt(`${basePage}/start`, {
    routers: {
      listItem: 'tr',
      name: 'routers',
      data: {
        brand: '.brand',
        model: '.model',
        page: {
          selector: '.device_page a',
          attr: 'href'
        },
        techData: {
          selector: '.device_techdata a',
          attr: 'href'
        }
      }
    }
  })
    .then(({ data, response }) => {
      console.log(`Status Code: ${response.statusCode}`)
      const filtered = data.routers.filter(i => i.page.length > 0)
      return filtered.map(i => {
        i.routerPath = i.techData
          .split('/toh/hwdata/')[1]
          .split('/')
          .join('~')

        return i
      })
    })
    .catch(err => {
      console.log('Error', err)
    })
}

async function scrapeRouterData (router) {
  const url = `${basePage}/hwdata/${router}`
  return scrapeIt(url, {
    name: 'h1',
    brand: '.brand',
    model: '.model',
    version: '.version',
    powerSupply: '.power_supply'
  })
    .then(({ data, response }) => {
      const name = data.name.split('OpenWrt ProjectTechdata: ')[1]
      const brand = data.brand.split('Brand: ')[1]
      const model = data.model.split('Model: ')[1]
      const versions = data.version.split('Version: ')[1]
      const powerSupplyData = data.powerSupply
        .split('Power Supply: ')[1]
        .split(',')
      const powerSupply = {
        volts: parseFloat(powerSupplyData[0].split(' ')[0]),
        amps: parseFloat(powerSupplyData[1].split(' ')[1])
      }
      console.log(`${name}: Status Code: ${response.statusCode}`)
      const formatedData = {
        name,
        brand,
        model,
        versions,
        powerSupply,
        url
      }
      if (!powerSupply.volts || !powerSupply.amps) return
      return formatedData
    })
    .catch(err => {
      console.log('Err on fetching router data', err)
    })
}

async function allRouters () {
  const list = {
    LibreRouter: [
      {
        name: 'Libre Router v1',
        model: 'Libre Router v1',
        versions: '1',
        powerSupply: {
          volts: 24,
          amps: 1
        },
        url: 'http://librerouter.org/'
      }
    ]
  }
  let routers = await scrapeRouterList()
  if (process.env.TEST) routers = routers.slice(0, 5)
  if (process.env.SYNC) {
    console.log('Fetching data in synchronous manner')
    for await (let router of routers) {
      try {
        const data = await scrapeRouterData(
          router.routerPath.split('~').join('/')
        )
        if (data) {
          if (list[data.brand]) list[data.brand].push(data)
          else list[data.brand] = [data]
        }
      } catch (err) {
        console.log('Error on getting router data')
      }
    }
  } else {
    await Promise.all(
      routers.map(async router => {
        try {
          const data = await throttle(
            scrapeRouterData(router.routerPath.split('~').join('/')),
            1000
          )
          if (data) {
            if (list[data.brand]) list[data.brand].push(data)
            else list[data.brand] = [data]
          }
        } catch (err) {
          console.log('Error on getting router data')
        }
      })
    )
  }

  return list
}

const storeData = async (data, path) => {
  try {
    await fs.writeFileSync(path, JSON.stringify(data))
  } catch (err) {
    console.error(err)
  }
}

async function run () {
  console.log('Starting!')
  const routerData = await allRouters()
  console.log('Done getting router data!')
  const dataDir = path.resolve(__dirname, '../src/assets/data/routers.json')
  await storeData(routerData, dataDir)
  console.log('Wrote to ', dataDir)
  console.log('Done!')
}

run()
