import Solcast from "solcast-unofficial";

const solcastApi = process.env.VUE_APP_SOLCAST_API_KEY;

const get = () => {
  console.log("solcastApi", solcastApi);

  var solcast = new Solcast(solcastApi, 1);
  var lat = -35.277;
  var long = 149.117;
  solcast
    .getRADForcasts(long, lat)
    .then(result => {
      // Log results
      console.log(result);
    })
    .catch(err => {
      console.log(err);
    });

  // Installation capacity in watts (e.g. 5 kW system)
  var capacity = 5000;

  // Gets PV generation forecast
  solcast
    .getPVEstimatedActuals(long, lat, capacity)
    .then(result => {
      // Log results
      console.log(result);
    })
    .catch(err => {
      console.log(err);
    });
};

export default get;
